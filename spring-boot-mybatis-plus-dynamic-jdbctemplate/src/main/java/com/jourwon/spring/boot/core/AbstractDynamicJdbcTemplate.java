package com.jourwon.spring.boot.core;

import org.apache.commons.lang3.StringUtils;

/**
 * 抽象动态JdbcTemplate
 *
 * @author JourWon
 * @date 2022/3/17
 */
public abstract class AbstractDynamicJdbcTemplate {

    private final String primaryDb;

    public AbstractDynamicJdbcTemplate(String primaryDb) {
        this.primaryDb = primaryDb;
    }

    protected String determineDatasource(String key) {
        if (StringUtils.isNotEmpty(key)) {
            return key;
        }
        return this.primaryDb;
    }

}
