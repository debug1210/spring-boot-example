package com.jourwon.spring.boot.config;

import com.baomidou.dynamic.datasource.spring.boot.autoconfigure.DynamicDataSourceProperties;
import com.jourwon.spring.boot.core.DynamicJdbcTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.annotation.Resource;

/**
 * 动态JdbcTemplate配置
 *
 * @author JourWon
 * @date 2022/3/17
 */
@Slf4j
@Configuration
public class DynamicJdbcTemplateConfig {

    @Resource
    private DynamicDataSourceProperties dynamicDataSourceProperties;

    @Bean
    public DynamicJdbcTemplate dynamicJdbcTemplate(JdbcTemplate jdbcTemplate) {
        return new DynamicJdbcTemplate(jdbcTemplate, this.dynamicDataSourceProperties.getPrimary());
    }

}
