package com.jourwon.spring.boot.controller;

import org.springframework.util.StopWatch;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * HelloWorldController
 *
 * @author JourWon
 * @date 2021/1/15
 */
@RestController
public class HelloWorldController {

    @GetMapping("/hello_world")
    public String helloWorld() {
        return "Hello World";
    }

    public static void main (String[] args) throws InterruptedException {
        StopWatch sw = new StopWatch();
        sw.start();
        //long task simulation
        Thread.sleep(100);
        sw.stop();
        System.out.println(sw.getTotalTimeMillis());

        sw.start();
        //long task simulation
        Thread.sleep(200);
        sw.stop();
        System.out.println(sw.getLastTaskTimeMillis());

        sw.start();
        //long task simulation
        Thread.sleep(300);
        sw.stop();
        System.out.println(sw.getLastTaskTimeMillis());
        System.out.println(sw.getTotalTimeMillis());
        System.out.println(sw.prettyPrint());
    }

}
