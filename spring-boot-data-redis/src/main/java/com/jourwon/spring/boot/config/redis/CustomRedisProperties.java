// package com.jourwon.spring.boot.config.redis;
//
// import lombok.Data;
// import lombok.Getter;
// import lombok.Setter;
// import org.springframework.boot.context.properties.ConfigurationProperties;
// import org.springframework.stereotype.Component;
//
// import java.time.Duration;
//
// /**
//  * Redis属性
//  *
//  * @author JourWon
//  * @date 2022/4/20
//  */
// @Data
// @Component
// @ConfigurationProperties(prefix = "custom.redis")
// public class CustomRedisProperties {
//
//     /**
//      * Redis运行模式
//      */
//     private String mode = RedisTypeEnum.STAND_ALONG.getValue();
//
//     /**
//      * 节点host:port配置
//      */
//     private String nodes = "localhost:6379";
//
//     /**
//      * 数据库索引
//      */
//     private int database = 0;
//
//     /**
//      * 用户名
//      */
//     private String username;
//
//     /**
//      * 密码
//      */
//     private String password;
//
//     /**
//      * 读取超时时间,单位毫秒
//      */
//     private Long timeout;
//
//     /**
//      * 连接超时时间,单位毫秒
//      */
//     private Long connectTimeout;
//
//     /**
//      * 集群模式有效,最大重定向次数
//      */
//     private Integer maxRedirects = 3;
//
//     /**
//      * 哨兵模式有效,主节点的名称
//      */
//     private String master;
//
//     /**
//      * 连接池配置
//      */
//     private Pool pool = new Pool();
//
//     @Getter
//     @Setter
//     public static class Pool {
//
//         /**
//          * 最大空闲连接数
//          */
//         private Integer maxIdle = 8;
//
//         /**
//          * 最小空闲连接数
//          */
//         private Integer minIdle = 0;
//
//         /**
//          * 最大连接数
//          */
//         private Integer maxActive = 8;
//
//         /**
//          * 当资源池连接用尽后，调用者的最大等待时间（单位为毫秒）。 使用负值来无限期地阻塞。
//          */
//         private Duration maxWait = Duration.ofMillis(60000);
//
//         /**
//          * 是否开启空闲资源监测,默认false
//          */
//         private Boolean testWhileIdle = true;
//
//         /**
//          * 资源池中资源最小空闲时间(单位为毫秒)，达到此值后空闲资源将被移除,默认30分钟
//          */
//         private Long minEvictableIdleTimeMs = 60000L;
//
//         /**
//          * 空闲资源的检测周期(单位为毫秒)，默认-1表示不检测
//          */
//         private Long timeBetweenEvictionRunsMs = 30000L;
//
//         /**
//          * 做空闲资源检测时，每次的采样数，默认3，-1表示对所有连接做空闲监测
//          */
//         private Integer numTestsPerEvictionRun = -1;
//
//     }
//
//     public enum RedisTypeEnum {
//
//         /**
//          * 单例节点
//          */
//         STAND_ALONG("0"),
//
//         /**
//          * 哨兵模式
//          */
//         SENTINEL("1"),
//
//         /**
//          * 集群
//          */
//         CLUSTER("2");
//
//         @Getter
//         private String value;
//
//         RedisTypeEnum(String value) {
//             this.value = value;
//         }
//
//     }
//
// }
