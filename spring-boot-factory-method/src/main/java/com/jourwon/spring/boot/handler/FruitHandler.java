package com.jourwon.spring.boot.handler;

/**
 * 水果类型处理器
 *
 * @author JourWon
 * @date 2022/3/24
 */
public interface FruitHandler<T> {

    /**
     * 数据类型
     *
     * @return
     */
    String getType();

    /**
     * 处理方法
     *
     * @param obj
     */
    void handle(T obj);

}
