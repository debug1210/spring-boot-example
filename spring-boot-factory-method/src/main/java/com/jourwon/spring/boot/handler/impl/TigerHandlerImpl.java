package com.jourwon.spring.boot.handler.impl;

import com.alibaba.fastjson.JSON;
import com.jourwon.spring.boot.enums.AnimalTypeEnum;
import com.jourwon.spring.boot.handler.AnimalHandler;
import com.jourwon.spring.boot.model.dto.Tiger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 老虎处理器
 *
 * @author JourWon
 * @date 2022/3/25
 */
@Slf4j
@Component
public class TigerHandlerImpl implements AnimalHandler<Tiger> {

    @Override
    public String getType() {
        return AnimalTypeEnum.TIGER.getType();
    }

    @Override
    public void handle(Tiger obj) {
        log.info("老虎处理方法,老虎对象:{}", JSON.toJSONString(obj));
    }

}
