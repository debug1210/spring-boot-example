package com.jourwon.spring.boot.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 香蕉
 *
 * @author JourWon
 * @date 2022/3/25
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Banana {

    /**
     * 重量
     */
    private Float weight;

    /**
     * 价格
     */
    private Float price;

}
