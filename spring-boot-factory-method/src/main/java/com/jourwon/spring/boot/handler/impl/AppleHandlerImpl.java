package com.jourwon.spring.boot.handler.impl;

import com.alibaba.fastjson.JSON;
import com.jourwon.spring.boot.enums.FruitTypeEnum;
import com.jourwon.spring.boot.handler.FruitHandler;
import com.jourwon.spring.boot.model.dto.Apple;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 苹果处理器
 *
 * @author JourWon
 * @date 2022/3/25
 */
@Slf4j
@Component
public class AppleHandlerImpl implements FruitHandler<Apple> {

    @Override
    public String getType() {
        return FruitTypeEnum.APPLE.getType();
    }

    @Override
    public void handle(Apple obj) {
        log.info("苹果处理方法,苹果对象:{}", JSON.toJSONString(obj));
    }

}
