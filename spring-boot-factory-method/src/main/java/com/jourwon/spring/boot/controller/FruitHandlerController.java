package com.jourwon.spring.boot.controller;

import com.jourwon.spring.boot.enums.FruitTypeEnum;
import com.jourwon.spring.boot.handler.FruitHandler;
import com.jourwon.spring.boot.handler.HandlerFactory;
import com.jourwon.spring.boot.model.dto.Apple;
import com.jourwon.spring.boot.model.dto.Banana;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 水果接口
 *
 * @author JourWon
 * @date 2022/3/25
 */
@RestController
@Api(tags = "水果接口")
@RequestMapping("fruit")
public class FruitHandlerController {

    @Resource
    private HandlerFactory<FruitHandler> factory;

    @GetMapping("/apple")
    @ApiOperation("获取苹果信息")
    public void apple() {
        factory.getHandler(FruitTypeEnum.APPLE.getType()).handle(new Apple(8.0F, "红色"));
    }

    @GetMapping("/banana")
    @ApiOperation("获取香蕉信息")
    public void banana() {
        factory.getHandler(FruitTypeEnum.BANANA.getType()).handle(new Banana(100F, 4.0F));
    }

}
