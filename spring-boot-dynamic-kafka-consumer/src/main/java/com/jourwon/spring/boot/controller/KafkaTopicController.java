package com.jourwon.spring.boot.controller;

import com.jourwon.spring.boot.util.KafkaTopicUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * kafka主题管理接口
 *
 * @author JourWon
 * @date 2022/3/22
 */
@Slf4j
@RestController
@Api(tags = "kafka主题管理接口")
@RequestMapping(path = "/api/kafka/topic")
public class KafkaTopicController {

    @Resource
    private KafkaTopicUtils kafkaTopicUtils;

    @GetMapping
    @ApiOperation("获取kafka主题列表")
    public List<String> list() {
        return kafkaTopicUtils.list();
    }

    @PostMapping
    @ApiOperation("创建kafka主题")
    public boolean createTopic(@RequestParam String topic) {
        return kafkaTopicUtils.createTopic(topic);
    }

    @DeleteMapping
    @ApiOperation("删除kafka主题")
    public boolean deleteTopic(@RequestParam String topic) {
        return kafkaTopicUtils.deleteTopic(topic);
    }

    @GetMapping(path = "/describeTopic")
    @ApiOperation("获取kafka主题详情")
    public String describeTopic(@RequestParam String topic) {
        return kafkaTopicUtils.describeTopic(topic).toString();
    }

}
