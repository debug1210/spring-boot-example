package com.jourwon.spring.boot.prop;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 自定义kafka监听者属性
 *
 * @author JourWon
 * @date 2022/3/21
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CustomKafkaListenerProperty {

    /**
     * 主题
     */
    private String topic;

    /**
     * 监听者类
     */
    private String listenerClass;

}
