package com.jourwon.spring.boot.service;

import com.jourwon.spring.boot.model.KafkaConsumerInfo;

import java.util.List;

/**
 * kafka消费者接口
 *
 * @author JourWon
 * @date 2022/3/22
 */
public interface KafkaConsumerRegistryService {

    /**
     * 获取kafka消费者列表
     *
     * @return
     */
    List<KafkaConsumerInfo> listConsumerId();

    /**
     * 注册kafka消费者
     *
     * @param topic
     * @param listenerClass
     * @param startImmediately
     * @return
     */
    String registeConsumer(String topic, String listenerClass, boolean startImmediately);

    /**
     * 启用kafka消费者
     *
     * @param consumerId
     */
    void startConsumer(String consumerId);

    /**
     * 暂停kafka消费者
     *
     * @param consumerId
     */
    void pauseConsumer(String consumerId);

    /**
     * 恢复kafka消费者
     *
     * @param consumerId
     */
    void resumeConsumer(String consumerId);

    /**
     * 停用kafka消费者
     *
     * @param consumerId
     */
    void stopConsumer(String consumerId);

}
