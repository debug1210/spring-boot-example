package com.jourwon.spring.boot.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jourwon.spring.boot.mapper.MyBatisPlusUserMapper;
import com.jourwon.spring.boot.model.dto.InsertUserDTO;
import com.jourwon.spring.boot.model.dto.UpdateUserDTO;
import com.jourwon.spring.boot.model.entity.User;
import com.jourwon.spring.boot.model.query.UserQuery;
import com.jourwon.spring.boot.model.vo.CommonPageVO;
import com.jourwon.spring.boot.model.vo.UserVO;
import com.jourwon.spring.boot.service.MyBatisPlusUserService;
import com.jourwon.spring.boot.util.BeanTransformUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户service实现类
 *
 * @author JourWon
 * @date 2022/6/28
 */
@Service
public class MyBatisPlusUserServiceImpl extends ServiceImpl<MyBatisPlusUserMapper, User> implements MyBatisPlusUserService {

    @Resource
    private MyBatisPlusUserMapper myBatisPlusUserMapper;

    @Override
    public CommonPageVO<UserVO> page1(UserQuery userQuery) {
        PageInfo<User> pageInfo = PageHelper.startPage(userQuery.getPageNum(), userQuery.getPageSize())
                .doSelectPageInfo(() -> myBatisPlusUserMapper.selectList(Wrappers.<User>lambdaQuery()
                        .likeRight(StringUtils.isNotBlank(userQuery.getUsername()), User::getUsername, userQuery.getUsername())
                        .likeRight(StringUtils.isNotBlank(userQuery.getMobilePhoneNumber()), User::getMobilePhoneNumber, userQuery.getMobilePhoneNumber())
                        .likeRight(StringUtils.isNotBlank(userQuery.getEmail()), User::getEmail, userQuery.getEmail())
                        .orderByDesc(User::getUserId)));

        List<UserVO> list = BeanTransformUtils.transformList(pageInfo.getList(), UserVO.class);
        CommonPageVO<UserVO> commonPageVO = new CommonPageVO<>(pageInfo.getPageNum(), pageInfo.getPageSize(),
                pageInfo.getSize(), pageInfo.getPages(), pageInfo.getTotal(), list);
        return commonPageVO;
    }

    @Override
    public CommonPageVO<UserVO> page2(UserQuery userQuery) {
        IPage<User> page = new Page<>(userQuery.getPageNum(), userQuery.getPageSize());
        IPage<User> userPage = this.baseMapper.selectPage(page, Wrappers.<User>lambdaQuery()
                .likeRight(StringUtils.isNotBlank(userQuery.getUsername()), User::getUsername, userQuery.getUsername())
                .likeRight(StringUtils.isNotBlank(userQuery.getMobilePhoneNumber()), User::getMobilePhoneNumber, userQuery.getMobilePhoneNumber())
                .likeRight(StringUtils.isNotBlank(userQuery.getEmail()), User::getEmail, userQuery.getEmail())
                .orderByDesc(User::getUserId));
        List<UserVO> list = BeanTransformUtils.transformList(userPage.getRecords(), UserVO.class);
        CommonPageVO<UserVO> commonPageVO = new CommonPageVO<>((int) userPage.getCurrent(), (int) userPage.getSize(),
                userPage.getRecords().size(), (int) userPage.getPages(), userPage.getTotal(), list);
        return commonPageVO;
    }

    @Override
    public CommonPageVO<UserVO> page3(UserQuery userQuery) {
        IPage<User> userPage = this.lambdaQuery()
                .likeRight(StringUtils.isNotBlank(userQuery.getUsername()), User::getUsername, userQuery.getUsername())
                .likeRight(StringUtils.isNotBlank(userQuery.getMobilePhoneNumber()), User::getMobilePhoneNumber, userQuery.getMobilePhoneNumber())
                .likeRight(StringUtils.isNotBlank(userQuery.getEmail()), User::getEmail, userQuery.getEmail())
                .orderByDesc(User::getUserId)
                .page(new Page<>(userQuery.getPageNum(), userQuery.getPageSize()));

        List<UserVO> list = BeanTransformUtils.transformList(userPage.getRecords(), UserVO.class);
        CommonPageVO<UserVO> commonPageVO = new CommonPageVO<>((int) userPage.getCurrent(), (int) userPage.getSize(),
                userPage.getRecords().size(), (int) userPage.getPages(), userPage.getTotal(), list);
        return commonPageVO;
    }


    @Override
    public boolean saveUser(InsertUserDTO insertUserDTO) {
        User user = BeanTransformUtils.transform(insertUserDTO, User.class);
        return this.save(user);
    }

    @Override
    public boolean updateUser(UpdateUserDTO updateUserDTO) {
        User user = BeanTransformUtils.transform(updateUserDTO, User.class);
        return this.updateById(user);
    }

    @Override
    public boolean removeUser(Long userId) {
        return this.removeById(userId);
    }

    @Override
    public List<UserVO> orderByMobilePhoneNumberDesc() {
        List<User> list = myBatisPlusUserMapper.selectList(Wrappers.<User>lambdaQuery()
                .select(User::getUsername, User::getMobilePhoneNumber, User::getEmail)
                .groupBy(User::getMobilePhoneNumber)
                .orderByDesc(User::getMobilePhoneNumber));
        return BeanTransformUtils.transformList(list, UserVO.class);
    }

}
