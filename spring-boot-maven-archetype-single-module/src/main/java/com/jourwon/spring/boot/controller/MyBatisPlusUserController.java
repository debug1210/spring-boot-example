package com.jourwon.spring.boot.controller;

import com.jourwon.spring.boot.model.dto.InsertUserDTO;
import com.jourwon.spring.boot.model.dto.UpdateUserDTO;
import com.jourwon.spring.boot.model.entity.User;
import com.jourwon.spring.boot.model.query.UserQuery;
import com.jourwon.spring.boot.model.vo.CommonPageVO;
import com.jourwon.spring.boot.model.vo.UserVO;
import com.jourwon.spring.boot.service.MyBatisPlusUserService;
import com.jourwon.spring.boot.util.BeanTransformUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

/**
 * 用户controller
 *
 * @author JourWon
 * @date 2021/2/6
 */
@Api(tags = "用户-使用MyBatis-Plus作为持久层,同时响应使用ResponseBodyAdvice包装")
@RestController
@RequestMapping("/mybatis-plus-users")
public class MyBatisPlusUserController {

    @Resource
    private MyBatisPlusUserService userService;

    @PostMapping
    @ApiOperation("新增用户1")
    public boolean insertSelective(@Valid @RequestBody InsertUserDTO insertUserDTO) {
        User user = BeanTransformUtils.transform(insertUserDTO, User.class);
        return userService.saveOrUpdate(user);
    }

    @PostMapping("/save-user")
    @ApiOperation("新增用户2")
    public boolean saveUser(@Valid @RequestBody InsertUserDTO insertUserDTO) {
        return userService.saveUser(insertUserDTO);
    }

    @DeleteMapping("/{userId}")
    @ApiOperation("根据主键删除用户1")
    public boolean removeById(@PathVariable("userId") Long userId) {
        return userService.removeById(userId);
    }

    @DeleteMapping("/remove-user/{userId}")
    @ApiOperation("根据主键删除用户2")
    public boolean removeUser(@PathVariable("userId") Long userId) {
        return userService.removeUser(userId);
    }

    @PutMapping("/{userId}")
    @ApiOperation("根据主键更新用户1")
    public boolean updateByPrimaryKeySelective(@PathVariable("userId") Long userId, @Valid @RequestBody UpdateUserDTO updateUserDTO) {
        updateUserDTO.setUserId(userId);
        User user = BeanTransformUtils.transform(updateUserDTO, User.class);
        return userService.saveOrUpdate(user);
    }

    @PutMapping("/update-user/{userId}")
    @ApiOperation("根据主键更新用户2")
    public boolean updateUser(@PathVariable("userId") Long userId, @Valid @RequestBody UpdateUserDTO updateUserDTO) {
        updateUserDTO.setUserId(userId);
        return userService.updateUser(updateUserDTO);
    }

    @GetMapping("/{userId}")
    @ApiOperation("通过主键获取用户")
    public UserVO getById(@PathVariable("userId") Long userId) {
        User user = userService.getById(userId);
        return BeanTransformUtils.transform(user, UserVO.class);
    }

    @GetMapping("/order-by-mobile-phone-number-desc")
    @ApiOperation("根据手机号码倒序查询用户列表")
    public List<UserVO> orderByMobilePhoneNumberDesc() {
        return userService.orderByMobilePhoneNumberDesc();
    }

    @GetMapping
    @ApiOperation("查询所有的用户列表")
    public List<UserVO> list() {
        List<User> list = userService.list();
        return BeanTransformUtils.transformList(list, UserVO.class);
    }

    @GetMapping("/page1")
    @ApiOperation("分页查询用户1")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "当前页数", dataTypeClass = Integer.class),
            @ApiImplicitParam(name = "pageSize", value = "每页记录数", dataTypeClass = Integer.class),
            @ApiImplicitParam(name = "username", value = "用户名", dataTypeClass = String.class),
            @ApiImplicitParam(name = "mobilePhoneNumber", value = "手机号码", dataTypeClass = String.class),
            @ApiImplicitParam(name = "email", value = "邮箱", dataTypeClass = String.class)
    })
    public CommonPageVO<UserVO> page1(@RequestParam(required = false, defaultValue = "1", value = "pageNum") Integer pageNum,
                                      @RequestParam(required = false, defaultValue = "10", value = "pageSize") Integer pageSize,
                                      @RequestParam(required = false, value = "username") String username,
                                      @RequestParam(required = false, value = "mobilePhoneNumber") String mobilePhoneNumber,
                                      @RequestParam(required = false, value = "email") String email) {
        UserQuery userQuery = UserQuery.builder().pageNum(pageNum).pageSize(pageSize).username(username)
                .mobilePhoneNumber(mobilePhoneNumber).email(email).build();
        return userService.page1(userQuery);
    }

    @GetMapping("/page2")
    @ApiOperation("分页查询用户2")
    public CommonPageVO<UserVO> page2(@Valid UserQuery userQuery) {
        return userService.page2(userQuery);
    }

    @GetMapping("/page3")
    @ApiOperation("分页查询用户3")
    public CommonPageVO<UserVO> page3(@Valid UserQuery userQuery) {
        return userService.page3(userQuery);
    }

}
