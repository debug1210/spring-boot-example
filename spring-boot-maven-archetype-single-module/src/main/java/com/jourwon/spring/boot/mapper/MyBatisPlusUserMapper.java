package com.jourwon.spring.boot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.jourwon.spring.boot.model.entity.User;

/**
 * 用户mapper
 *
 * @author JourWon
 * @date 2022/6/28
 */
public interface MyBatisPlusUserMapper extends BaseMapper<User> {
}
