package com.jourwon.spring.boot.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.jourwon.spring.boot.mapper.UserMapper;
import com.jourwon.spring.boot.model.dto.InsertUserDTO;
import com.jourwon.spring.boot.model.dto.UpdateUserDTO;
import com.jourwon.spring.boot.model.dto.UserDTO;
import com.jourwon.spring.boot.model.entity.User;
import com.jourwon.spring.boot.model.query.UserQuery;
import com.jourwon.spring.boot.model.vo.CommonPageVO;
import com.jourwon.spring.boot.model.vo.UserVO;
import com.jourwon.spring.boot.service.UserService;
import com.jourwon.spring.boot.util.BeanTransformUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户service实现类
 *
 * @author JourWon
 * @date 2021/2/6
 */
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserMapper userMapper;

    @Override
    public int insert(InsertUserDTO insertUserDTO) {
        User user = BeanTransformUtils.transform(insertUserDTO, User.class);
        return userMapper.insert(user);
    }

    @Override
    public int insertSelective(InsertUserDTO insertUserDTO) {
        User user = BeanTransformUtils.transform(insertUserDTO, User.class);
        return userMapper.insertSelective(user);
    }

    @Override
    public int deleteByPrimaryKey(Long userId) {
        return userMapper.deleteByPrimaryKey(userId);
    }

    @Override
    public int updateByPrimaryKey(UpdateUserDTO updateUserDTO) {
        User user = BeanTransformUtils.transform(updateUserDTO, User.class);
        return userMapper.updateByPrimaryKey(user);
    }

    @Override
    public int updateByPrimaryKeySelective(UpdateUserDTO updateUserDTO) {
        User user = BeanTransformUtils.transform(updateUserDTO, User.class);
        return userMapper.updateByPrimaryKeySelective(user);
    }

    @Override
    public UserDTO getByPrimaryKey(Long userId) {
        User user = userMapper.getByPrimaryKey(userId);
        if (null == user) {
            throw new RuntimeException("查不到userId为" + userId + "对应的用户");
        }

        return BeanTransformUtils.transform(user, UserDTO.class);
    }

    @Override
    public List<UserDTO> listUsers() {
        List<User> listUsers = userMapper.listUsers();
        return BeanTransformUtils.transformList(listUsers, UserDTO.class);
    }

    @Override
    public CommonPageVO<UserVO> page(UserQuery userQuery) {
        User user = BeanTransformUtils.transform(userQuery, User.class);
        PageInfo<User> pageInfo = PageHelper.startPage(userQuery.getPageNum(), userQuery.getPageSize())
                .doSelectPageInfo(() -> userMapper.page(user));

        List<UserVO> list = BeanTransformUtils.transformList(pageInfo.getList(), UserVO.class);
        return new CommonPageVO<>(pageInfo.getPageNum(), pageInfo.getPageSize(), pageInfo.getSize(), pageInfo.getPages(), pageInfo.getTotal(), list);
    }

}
