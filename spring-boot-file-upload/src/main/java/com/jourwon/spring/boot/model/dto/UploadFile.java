package com.jourwon.spring.boot.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 上传文件信息
 *
 * @author JourWon
 * @date 2021/12/28
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UploadFile {

    /**
     * 文件名
     */
    private String fileName;

    /**
     * 文件url
     */
    private String url;

}
