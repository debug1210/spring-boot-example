package com.jourwon.spring.boot.runner;

import com.jourwon.spring.boot.service.FileStorageService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * 初始化文件存储空间
 *
 * @author JourWon
 * @date 2021/12/28
 */
@Configuration
public class FileUploadRunner implements CommandLineRunner {

    @Resource
    FileStorageService fileStorageService;

    @Override
    public void run(String... args) {
        // 项目启动时删除文件夹里面的文件
        // fileStorageService.clear();
        // 创建文件夹
        fileStorageService.init();
    }

}
