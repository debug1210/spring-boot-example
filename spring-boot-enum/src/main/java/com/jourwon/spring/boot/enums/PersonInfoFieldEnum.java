package com.jourwon.spring.boot.enums;

import com.jourwon.spring.boot.service.FieldInfoService;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author JourWon
 * @date 2022/6/1
 */
@Getter
@AllArgsConstructor
public enum PersonInfoFieldEnum implements FieldInfoService {

    /**
     * 人体编码
     */
    PERSON_ID("PERSON_ID", "人体编码"),

    /**
     * 身高
     */
    HEIGHT("HEIGHT", "身高"),

    ;

    /**
     * 字段名称
     */
    private final String fieldName;

    /**
     * 字段描述
     */
    private final String fieldDesc;

}
