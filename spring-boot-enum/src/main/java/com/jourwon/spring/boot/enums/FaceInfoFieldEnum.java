package com.jourwon.spring.boot.enums;

import com.jourwon.spring.boot.service.FieldInfoService;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author JourWon
 * @date 2022/6/1
 */
@Getter
@AllArgsConstructor
public enum FaceInfoFieldEnum implements FieldInfoService {

    /**
     * 人脸编码
     */
    FACE_ID("FACE_ID", "人脸编码"),

    /**
     * 设备编码
     */
    DEVICE_ID("DEVICE_ID", "设备编码"),

    ;

    /**
     * 字段名称
     */
    private final String fieldName;

    /**
     * 字段描述
     */
    private final String fieldDesc;

}
