package com.jourwon.spring.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author JourWon
 * @date 2022/6/2
 */
@SpringBootApplication
public class SpringBootEnumApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootEnumApplication.class, args);
    }

}
