package com.jourwon.spring.boot.util;

import com.jourwon.spring.boot.model.dto.FieldInfo;
import com.jourwon.spring.boot.service.FieldInfoService;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author JourWon
 * @date 2022/6/1
 */
public class FieldInfoUtils {

    public static List<FieldInfo> getFieldInfo(FieldInfoService[] fieldArray) {
        return Arrays.stream(fieldArray).map(FieldInfo::new).collect(Collectors.toList());
    }

}
