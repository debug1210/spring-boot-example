package com.jourwon.spring.boot;

import com.alibaba.fastjson.JSON;
import com.jourwon.spring.boot.util.EasticsearchUtils;
import com.jourwon.spring.boot.util.ElasticsearchDocument;
import lombok.extern.slf4j.Slf4j;
import org.elasticsearch.index.query.TermQueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@SpringBootTest
public class SpringBootDataElasticsearchApplicationTests {

    @Resource
    private EasticsearchUtils easticsearchUtils;

    @Test
    public void test01() {
        System.out.println(easticsearchUtils.existsIndex("a"));
        System.out.println(easticsearchUtils.existsIndex("kibana_sample_data_flights"));
    }

    @Test
    public void test02() {
        System.out.println(easticsearchUtils.createIndex("test"));
        System.out.println(easticsearchUtils.existsIndex("test"));
    }

    @Test
    public void test03() {
        Map<String, Map<String, Object>> properties = new HashMap<>();
        Map<String, Object> map = new HashMap<>();
        map.put("type", "text");
        properties.put("name", map);

        System.out.println(easticsearchUtils.createIndex("test01", properties));
        System.out.println(easticsearchUtils.existsIndex("test01"));
    }

    @Test
    public void test04() {
        System.out.println(easticsearchUtils.deleteIndex("test01"));
        System.out.println(easticsearchUtils.existsIndex("test01"));
    }

    @Test
    public void test05() {
        System.out.println(easticsearchUtils.existsIndex("kibana_sample_data_flights"));
        System.out.println(easticsearchUtils.getSettings("kibana_sample_data_flights"));
    }

    @Test
    public void test06() {
        Map<String, String> map = new HashMap<>();
        map.put("key1", "value1");
        map.put("key2", "value2");

        System.out.println(easticsearchUtils.insertDocument("test", map));
        System.out.println(easticsearchUtils.insertOrUpdateDocument("test", "doc1", map));
        System.out.println(easticsearchUtils.existsDocument("test", "doc1"));
    }

    @Test
    public void test07() {
        Map<String, String> map = new HashMap<>();
        map.put("key1", "value11");
        map.put("key2", "value22");

        System.out.println(easticsearchUtils.insertOrUpdateDocument("test", "doc1", map));
        System.out.println(easticsearchUtils.existsDocument("test", "doc1"));
    }

    @Test
    public void test08() {
        Map<String, String> map1 = new HashMap<>();
        map1.put("testkey1", "value1");
        Map<String, String> map2 = new HashMap<>();
        map2.put("testkey2", "value2");

        // ElasticsearchDocument对象的data字段需要是一个json字段,否则会报异常NotXContentException: Compressor detection can only be called on some xcontent bytes or compressed xcontent bytes
        List<ElasticsearchDocument<?>> list = new ArrayList<>();
        list.add(new ElasticsearchDocument<>("key01", map1));
        list.add(new ElasticsearchDocument<>("key02", map2));

        System.out.println(JSON.toJSONString(easticsearchUtils.batchInsertOrUpdateDocument("test", list)));
        System.out.println(easticsearchUtils.existsDocument("test", "key01"));
        System.out.println(easticsearchUtils.existsDocument("test", "key02"));
    }

    @Test
    public void test09() {
        Map<String, String> map1 = new HashMap<>();
        map1.put("testkey1", "update1");
        Map<String, String> map2 = new HashMap<>();
        map2.put("testkey2", "udpate2");

        System.out.println(easticsearchUtils.updateById("test", "key01", map1));
        System.out.println(easticsearchUtils.updateByIdSelective("test", "key02", map2));
        System.out.println(easticsearchUtils.getById("test", "key01", JSON.class));
        System.out.println(easticsearchUtils.getById("test", "key02", JSON.class));
    }

    @Test
    public void test10() {
        Map<String, String> map1 = new HashMap<>();
        map1.put("testkey1", "update1");
        Map<String, String> map2 = new HashMap<>();
        map2.put("testkey2", "udpate2");

        List<String> idList = new ArrayList<>();
        idList.add("04AXMn4BSzS6WH-3Nde1");
        idList.add("11");

        System.out.println(easticsearchUtils.deleteById("test", "key01"));
        System.out.println(JSON.toJSONString(easticsearchUtils.deleteByIdList("test", idList)));
        System.out.println(JSON.toJSONString(easticsearchUtils.deleteByQuery("test", new TermQueryBuilder("user", "kimchy"))));
        System.out.println(easticsearchUtils.existsDocument("test", "key01"));
        System.out.println(easticsearchUtils.existsDocument("test", "04AXMn4BSzS6WH-3Nde1"));
        System.out.println(easticsearchUtils.existsDocument("test", "11"));
    }

    @Test
    public void test11() {
        List<String> idList = new ArrayList<>();
        idList.add("doc1");
        idList.add("key03");
        idList.add("key02");

        System.out.println(easticsearchUtils.getById("test", "1IAZMn4BSzS6WH-3OddL", JSON.class));
        System.out.println(easticsearchUtils.getByIdList("test", idList, JSON.class));
    }

    @Test
    public void test12() {
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.from(0);
        searchSourceBuilder.size(2);

        System.out.println(easticsearchUtils.search("test", 1, 1, JSON.class));
        System.out.println(easticsearchUtils.search("test", searchSourceBuilder, JSON.class));
    }

}
