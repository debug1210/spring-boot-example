package com.jourwon.spring.boot.util;

import lombok.Data;

import java.util.Collections;
import java.util.List;

/**
 * Elasticsearch scroll查询结果
 *
 * @author JourWon
 * @date 2023/4/27
 */
@Data
public final class ElasticsearchScrollResult<T> {

    /**
     * 是否成功，true表示成功，false表示失败
     */
    private boolean success;

    /**
     * 当前查询的scrollId
     */
    private String currentScrollId;

    /**
     * 下一次查询的scrollId
     */
    private String nextScrollId;

    /**
     * 结果
     */
    private List<T> list;

    public static <T> ElasticsearchScrollResult<T> ofSuccess(String currentScrollId, String nextScrollId, List<T> list) {
        ElasticsearchScrollResult<T> elasticsearchScrollResult = new ElasticsearchScrollResult<>();

        elasticsearchScrollResult.setCurrentScrollId(currentScrollId);
        elasticsearchScrollResult.setNextScrollId(nextScrollId);
        elasticsearchScrollResult.setList(list);
        elasticsearchScrollResult.setSuccess(true);

        return elasticsearchScrollResult;
    }

    public static <T> ElasticsearchScrollResult<T> ofEmpty(String currentScrollId) {
        ElasticsearchScrollResult<T> elasticsearchScrollResult = new ElasticsearchScrollResult<>();

        elasticsearchScrollResult.setCurrentScrollId(currentScrollId);
        elasticsearchScrollResult.setNextScrollId(null);
        elasticsearchScrollResult.setList(Collections.emptyList());
        elasticsearchScrollResult.setSuccess(false);

        return elasticsearchScrollResult;
    }

}
