package com.jourwon.spring.boot.util;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.DocWriteResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.settings.get.GetSettingsRequest;
import org.elasticsearch.action.admin.indices.settings.get.GetSettingsResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.*;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.ClearScrollRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchScrollRequest;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.core.CountRequest;
import org.elasticsearch.client.core.CountResponse;
import org.elasticsearch.client.indices.*;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentBuilder;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.reindex.DeleteByQueryRequest;
import org.elasticsearch.index.reindex.UpdateByQueryRequest;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.script.Script;
import org.elasticsearch.search.Scroll;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.fetch.subphase.FetchSourceContext;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Easticsearch 工具类
 * 注：如果需要将查询结果映射到实体类，可以使用hutool工具类的BeanUtil.setFieldValue(t, idField, getResponse.getId());方法
 *
 * @author JourWon
 * @date 2021/12/31
 */
@Slf4j
@Component
public class EasticsearchUtils {

    /**
     * 默认主分片数
     */
    private static final int DEFAULT_NUMBER_OF_SHARDS = 3;

    /**
     * 默认副本分片数
     */
    private static final int DEFAULT_NUMBER_OF_REPLICAS = 2;

    /**
     * 默认第一页
     */
    private static final int PAGE_NUM = 1;

    /**
     * 默认一页10条
     */
    private static final int PAGE_SIZE = 10;

    @Resource
    private RestHighLevelClient restHighLevelClient;

    private void assertIndex(String index) {
        Assert.hasText(index, "索引不能为空");
    }

    private void assertIndexAndId(String index, String id) {
        Assert.hasText(index, "索引不能为空");
        Assert.hasText(id, "文档ID不能为空");
    }

    private void assertDocument(String index, Object doc) {
        Assert.hasText(index, "索引不能为空");
        Assert.notNull(doc, "文档内容不能为空");
    }

    private void assertIndexAndList(String index, List<?> documentList) {
        Assert.hasText(index, "索引不能为空");
        Assert.notEmpty(documentList, "文档列表不能为空");
    }

    private void assertDocument(String index, String id, Object doc) {
        Assert.hasText(index, "索引不能为空");
        Assert.hasText(id, "文档ID不能为空");
        Assert.notNull(doc, "文档内容不能为空");
    }

    // ======================== Index 索引 ========================

    /**
     * 判断索引是否存在
     *
     * @param index 索引
     * @return boolean true:存在, false:不存在
     */
    public boolean existsIndex(String index) {
        assertIndex(index);

        GetIndexRequest request = new GetIndexRequest(index);
        request.local(false);
        request.humanReadable(true);
        request.includeDefaults(false);

        try {
            return restHighLevelClient.indices().exists(request, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.error("[Elasticsearch] >> existsIndex exception,index:{}", index, e);
            return false;
        }
    }

    /**
     * 创建索引
     *
     * @param index 索引
     * @return boolean true:成功, false:失败
     */
    public boolean createIndex(String index) {
        assertIndex(index);

        if (existsIndex(index)) {
            log.error("Index:[{}] is exits!", index);
            return false;
        }
        // 创建索引请求
        CreateIndexRequest request = new CreateIndexRequest(index);
        try {
            // 执行客户端请求
            CreateIndexResponse response = restHighLevelClient.indices().create(request, RequestOptions.DEFAULT);
            return response.isAcknowledged();
        } catch (IOException e) {
            log.error("[Elasticsearch] >> createIndex exception,index:{}", index, e);
            return false;
        }
    }

    /**
     * 创建索引
     *
     * @param index            索引
     * @param properties       文档属性集合
     * @param numberOfShards   分片数
     * @param numberOfReplicas 副本数
     * @return boolean true:成功, false:失败
     */
    public boolean createIndex(String index, Map<String, Map<String, Object>> properties, Integer numberOfShards, Integer numberOfReplicas) {
        assertIndex(index);

        if (existsIndex(index)) {
            log.error("Index:[{}] is exits!", index);
            return false;
        }
        try {
            XContentBuilder builder = XContentFactory.jsonBuilder();
            // 注: ES 7.x 后的版本中,已经弃用 type
            builder.startObject()
                    .startObject("mappings")
                    .field("properties", properties)
                    .endObject()
                    .startObject("settings")
                    // 分片数
                    .field("number_of_shards", numberOfShards)
                    // 副本数
                    .field("number_of_replicas", numberOfReplicas)
                    .endObject()
                    .endObject();
            // 创建索引请求
            CreateIndexRequest request = new CreateIndexRequest(index).source(builder);
            // 执行客户端请求
            CreateIndexResponse response = restHighLevelClient.indices().create(request, RequestOptions.DEFAULT);
            return response.isAcknowledged();
        } catch (IOException e) {
            log.error("[Elasticsearch] >> createIndex exception,index:{},properties:{}", index, properties, e);
            return false;
        }
    }

    /**
     * 删除索引
     *
     * @param index 索引
     * @return boolean true:成功, false:失败
     */
    public boolean deleteIndex(String index) {
        assertIndex(index);

        if (!existsIndex(index)) {
            log.error("Index:[{}] is not exits!", index);
            return false;
        }
        try {
            DeleteIndexRequest request = new DeleteIndexRequest(index);
            AcknowledgedResponse response = restHighLevelClient.indices().delete(request, RequestOptions.DEFAULT);

            return response.isAcknowledged();
        } catch (IOException e) {
            log.error("[Elasticsearch] >> deleteIndex exception,index:{}", index, e);
            return false;
        }
    }

    /**
     * 获取索引配置
     *
     * @param index 索引
     * @return GetSettingsResponse 返回索引配置信息
     */
    public GetSettingsResponse getSettings(String index) {
        assertIndex(index);

        if (!existsIndex(index)) {
            log.error("Index:[{}] is not exits!", index);
            return null;
        }
        try {
            GetSettingsRequest request = new GetSettingsRequest().indices(index);
            return restHighLevelClient.indices().getSettings(request, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.error("[Elasticsearch] >> getSettings exception,index:{}", index, e);
            return null;
        }
    }

    /**
     * 获取索引映射信息
     *
     * @param index 索引
     * @return GetMappingsResponse 返回索引映射信息
     */
    public GetMappingsResponse getMapping(String index) {
        assertIndex(index);

        if (!existsIndex(index)) {
            log.error("Index:[{}] is not exits!", index);
            return null;
        }
        try {
            GetMappingsRequest request = new GetMappingsRequest().indices(index);
            return restHighLevelClient.indices().getMapping(request, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.error("[Elasticsearch] >> getMapping exception,index:{}", index, e);
            return null;
        }
    }


    // ======================== Document 文档 ========================

    /**
     * 通过ID判断文档是否存在
     *
     * @param index 索引
     * @param id    文档id
     * @return boolean true:存在, false:不存在
     */
    public boolean exists(String index, String id) {
        assertIndexAndId(index, id);

        try {
            GetRequest request = new GetRequest(index, id);
            // 禁用获取_source
            request.fetchSourceContext(new FetchSourceContext(false));
            // 禁用获取存储的字段
            request.storedFields("_none_");

            return restHighLevelClient.exists(request, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.error("[Elasticsearch] >> existsDocument exception,index:{},id:{}", index, id, e);
            return false;
        }
    }

    /**
     * 保存文档-随机生成文档ID
     *
     * @param index 索引
     * @param doc   文档
     * @return boolean true:成功, false:失败
     */
    public boolean insert(String index, Object doc) {
        assertDocument(index, doc);

        try {
            String docJson = JSON.toJSONString(doc);
            IndexRequest request = new IndexRequest(index);
            request.source(docJson, XContentType.JSON);
            restHighLevelClient.index(request, RequestOptions.DEFAULT);
            return true;
        } catch (IOException e) {
            log.error("[Elasticsearch] >> insert exception,index:{}", index, e);
            return false;
        }
    }

    /**
     * 保存文档-自定义文档ID
     * 如果文档存在,则更新文档;如果文档不存在,则保存文档
     *
     * @param index 索引
     * @param id    文档ID
     * @param doc   文档
     * @return boolean true:成功, false:失败
     */
    public boolean insert(String index, String id, Object doc) {
        assertDocument(index, id, doc);

        try {
            String docJson = JSON.toJSONString(doc);
            IndexRequest request = new IndexRequest(index);
            request.id(id);
            request.source(docJson, XContentType.JSON);
            restHighLevelClient.index(request, RequestOptions.DEFAULT);
            return true;
        } catch (IOException e) {
            log.error("[Elasticsearch] >> insert exception,index:{},id:{}", index, id, e);
            return false;
        }
    }

    /**
     * 批量-新增或保存文档
     * 如果集合中有些文档已经存在,则更新文档;不存在,则保存文档
     *
     * @param index        索引
     * @param documentList 文档集合
     * @return boolean true:成功, false:失败
     */
    public boolean batchInsertOrUpdate(String index, List<ElasticsearchDocument<?>> documentList) {
        assertIndexAndList(index, documentList);

        try {
            // 批量请求
            BulkRequest bulkRequest = new BulkRequest();
            documentList.forEach(doc -> bulkRequest.add(new IndexRequest(index)
                    .id(doc.getId())
                    .source(JSON.toJSONString(doc.getData()), XContentType.JSON)));
            return !restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT).hasFailures();
        } catch (IOException e) {
            log.error("[Elasticsearch] >> batchInsertOrUpdateDocument exception,index:{}", index, e);
            return false;
        }
    }

    /**
     * 根据ID删除文档
     *
     * @param index 索引
     * @param id    文档ID
     * @return boolean true:成功, false:失败
     */
    public boolean deleteById(String index, String id) {
        assertIndexAndId(index, id);

        try {
            DeleteRequest request = new DeleteRequest(index, id);
            DeleteResponse response = restHighLevelClient.delete(request, RequestOptions.DEFAULT);
            // 未找到文件
            if (response.getResult() == DocWriteResponse.Result.NOT_FOUND) {
                log.error("[Elasticsearch] >> deleteById document is not found, index:{},id:{}", index, id);
                return false;
            }
            return RestStatus.OK.equals(response.status());
        } catch (IOException e) {
            log.error("[Elasticsearch] >> deleteById exception,index:{},id:{}", index, id, e);
            return false;
        }
    }

    /**
     * 根据查询条件删除文档
     *
     * @param index        索引
     * @param queryBuilder 查询条件构建器
     * @return boolean true:成功, false:失败
     */
    public boolean deleteByQuery(String index, QueryBuilder queryBuilder) {
        assertIndex(index);

        try {
            DeleteByQueryRequest request = new DeleteByQueryRequest(index).setQuery(queryBuilder);
            // 设置版本冲突时继续
            request.setAbortOnVersionConflict(false);
            // 删除文档之后调用索引刷新
            request.setRefresh(true);
            restHighLevelClient.deleteByQuery(request, RequestOptions.DEFAULT);
            return true;
        } catch (IOException e) {
            log.error("[Elasticsearch] >> deleteByQuery exception,index:{}", index);
            return false;
        }
    }

    /**
     * 根据文档 ID 批量删除文档
     *
     * @param index  索引
     * @param idList 文档 ID 集合
     * @return boolean true:成功, false:失败
     */
    public boolean deleteByIdList(String index, List<String> idList) {
        assertIndexAndList(index, idList);

        try {
            BulkRequest bulkRequest = new BulkRequest();
            idList.forEach(id -> bulkRequest.add(new DeleteRequest(index, id)));
            return !restHighLevelClient.bulk(bulkRequest, RequestOptions.DEFAULT).hasFailures();
        } catch (IOException e) {
            log.error("[Elasticsearch] >> deleteByIdList exception,index:{}", index, e);
            return false;
        }
    }

    /**
     * 根据ID修改文档
     *
     * @param index 索引
     * @param id    文档ID
     * @param doc   文档
     * @return boolean true:成功, false:失败
     */
    public boolean updateById(String index, String id, Object doc) {
        assertDocument(index, id, doc);

        try {
            String docJson = JSON.toJSONString(doc);
            UpdateRequest request = new UpdateRequest(index, id);
            request.doc(docJson, XContentType.JSON);
            restHighLevelClient.update(request, RequestOptions.DEFAULT);
            return true;
        } catch (IOException e) {
            log.error("[Elasticsearch] >> updateById exception,index:{},id:{}", index, id, e);
            return false;
        }
    }

    /**
     * 根据ID修改文档
     * 注:
     * 1).可变更已有字段值,可新增字段
     * 2).若当前ID文档不存在则新增文档
     *
     * @param index 索引
     * @param id    文档ID
     * @param doc   文档
     * @return boolean true:成功, false:失败
     */
    public boolean updateOrInsertById(String index, String id, Object doc) {
        assertDocument(index, id, doc);

        try {
            String docJson = JSON.toJSONString(doc);
            JSONObject jsonObject = JSON.parseObject(docJson);
            UpdateRequest request = new UpdateRequest(index, id)
                    .doc(jsonObject)
                    .upsert(jsonObject);
            restHighLevelClient.update(request, RequestOptions.DEFAULT);
            return true;
        } catch (IOException e) {
            log.error("[Elasticsearch] >> updateByIdSelective exception,index:{},id:{}", index, id, e);
            return false;
        }
    }

    public boolean updateByQuery(String index, QueryBuilder queryBuilder, Script script) {
        if (StringUtils.isNotEmpty(index) && Objects.nonNull(queryBuilder) && Objects.nonNull(script)) {
            try {
                UpdateByQueryRequest updateByQueryRequest = new UpdateByQueryRequest(index);
                updateByQueryRequest.setQuery(queryBuilder);
                updateByQueryRequest.setScript(script);

                this.restHighLevelClient.updateByQuery(updateByQueryRequest, RequestOptions.DEFAULT);

            } catch (IOException e) {
                log.error("[Elasticsearch] >> updateByQuery exception,index:{}", index, e);
                return false;
            }
        }
        return false;
    }

    /**
     * 根据id查询
     *
     * @param index 索引
     * @param id    文档ID
     * @return T
     */
    public <T> T getById(String index, String id, Class<T> clazz) {
        assertIndexAndId(index, id);

        GetResponse getResponse = this.getById(index, id);
        if (Objects.isNull(getResponse)) {
            return null;
        }
        return JSON.parseObject(getResponse.getSourceAsString(), clazz);
    }

    /**
     * 根据id查询
     *
     * @param index 索引
     * @param id    文档ID
     * @return GetResponse
     */
    private GetResponse getById(String index, String id) {
        assertIndexAndId(index, id);

        GetRequest request = new GetRequest(index, id);
        try {
            return restHighLevelClient.get(request, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.error("[Elasticsearch] >> getById exception,index:{},id:{}", index, id, e);
            return null;
        }
    }

    /**
     * 根据id集-批量获取文档
     * 因为需要转换为指定类型,获取的类型需要是相同类型,索引指定的文档ID集对应的文档应该是相同类型的
     * 注意:文档ID不存在的会被忽略
     *
     * @param index  索引
     * @param idList 文档ID集
     * @return T List<T>
     */
    public <T> List<T> getByIdList(String index, List<String> idList, Class<T> clazz) {
        assertIndexAndList(index, idList);

        MultiGetItemResponse[] responses = this.getByIdList(index, idList);
        if (null == responses || responses.length == 0) {
            return Collections.emptyList();
        }

        List<T> resultList = new ArrayList<>(responses.length);
        for (MultiGetItemResponse response : responses) {
            GetResponse getResponse = response.getResponse();
            if (!getResponse.isExists()) {
                continue;
            }
            resultList.add(JSON.parseObject(getResponse.getSourceAsString(), clazz));
        }

        return resultList;
    }

    /**
     * 根据id集-批量获取文档
     *
     * @param index  索引
     * @param idList 文档ID集
     * @return MultiGetItemResponse[]
     */
    private MultiGetItemResponse[] getByIdList(String index, List<String> idList) {
        assertIndexAndList(index, idList);

        try {
            MultiGetRequest request = new MultiGetRequest();
            for (String id : idList) {
                request.add(new MultiGetRequest.Item(index, id));
            }

            // 同步执行
            MultiGetResponse responses = restHighLevelClient.mget(request, RequestOptions.DEFAULT);
            return responses.getResponses();
        } catch (IOException e) {
            log.error("[Elasticsearch] >> getByIdList exception,index:{}", index, e);
            return null;
        }
    }

    /**
     * 根据多条件查询--分页
     * 注:from-size -[ "浅"分页 ]
     *
     * @param index    索引
     * @param pageNo   页码
     * @param pageSize 页面大小 - Elasticsearch默认配置单次最大限制10000
     */
    public <T> List<T> search(String index, Integer pageNo, Integer pageSize, Class<T> clazz) {
        assertIndex(index);

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.from(getStartRow(pageNo, pageSize));
        searchSourceBuilder.size(pageSize);

        return this.search(index, searchSourceBuilder, clazz);
    }

    /**
     * 获取指定页码和分页大小的起始记录数
     *
     * @param pageNum  页码
     * @param pageSize 分页大小
     * @return Integer 指定页码和分页大小的起始记录数
     */
    public static Integer getStartRow(Integer pageNum, Integer pageSize) {
        if (Objects.isNull(pageNum)) {
            pageNum = PAGE_NUM;
        }
        if (Objects.isNull(pageSize)) {
            pageSize = PAGE_SIZE;
        }

        int startRow = pageSize * (pageNum - 1);
        return Math.max(startRow, 0);
    }

    /**
     * 条件查询
     *
     * @param index         索引
     * @param sourceBuilder 条件查询构建器
     * @param <T>           数据类型
     * @return List<T> 列表
     */
    public <T> List<T> search(String index, SearchSourceBuilder sourceBuilder, Class<T> clazz) {
        assertIndex(index);

        try {
            // 构建查询请求
            SearchRequest searchRequest = new SearchRequest(index).source(sourceBuilder);
            // 获取返回值
            SearchResponse response = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
            SearchHit[] hits = response.getHits().getHits();
            if (null == hits || hits.length == 0) {
                return Collections.emptyList();
            }

            List<T> resultList = new ArrayList<>(hits.length);
            for (SearchHit hit : hits) {
                resultList.add(JSON.parseObject(hit.getSourceAsString(), clazz));
            }
            return resultList;
        } catch (IOException e) {
            log.error("[Elasticsearch] >> search exception,index:{}", index, e);
            return Collections.emptyList();
        }
    }

    public long count(String index, QueryBuilder query) {
        assertIndex(index);

        CountRequest request = new CountRequest(index);
        request.query(query);
        try {
            CountResponse response = this.restHighLevelClient.count(request, RequestOptions.DEFAULT);
            return response.getCount();
        } catch (Exception e) {
            log.error("[Elasticsearch] >> count exception,index:{}", index, e);
            return 0L;
        }
    }

    public <T> ElasticsearchScrollResult<T> scroll(String index, String scrollId, Long keepAliveSeconds, QueryBuilder queryBuilder,
                                                   Integer batchSize, String[] includes, String[] excludes, Class<T> clazz) {
        try {
            SearchResponse searchResponse = getScrollResponse(new String[]{index}, queryBuilder,
                    batchSize, includes, excludes, scrollId, keepAliveSeconds);
            if (searchResponse == null || searchResponse.getHits() == null || searchResponse.getHits().getHits().length == 0) {
                if (searchResponse != null) {
                    clearScroll(searchResponse.getScrollId());
                }
                log.warn("[Elasticsearch] >> scroll查询响应结果没有数据,index:{},scrollId:{}", index, scrollId);

                return ElasticsearchScrollResult.ofEmpty(scrollId);
            }

            final String nextScrollId = searchResponse.getScrollId();
            final List<T> resultList = searchHitsToList(searchResponse.getHits().getHits(), clazz);

            return ElasticsearchScrollResult.ofSuccess(scrollId, nextScrollId, resultList);
        } catch (Exception e) {
            log.error("[Elasticsearch] >> scroll exception,scrollId:{}", scrollId, e);
            clearScroll(scrollId);
        }

        return ElasticsearchScrollResult.ofEmpty(scrollId);
    }

    private <T> List<T> searchHitsToList(SearchHit[] hits, Class<T> clazz) {
        if (hits == null || hits.length == 0) {
            return Collections.emptyList();
        }

        return Arrays.stream(hits)
                .map(hit -> JSON.parseObject(hit.getSourceAsString(), clazz))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public void clearScroll(String scrollId) {
        if (StringUtils.isBlank(scrollId)) {
            return;
        }

        ClearScrollRequest clearScrollRequest = new ClearScrollRequest();
        clearScrollRequest.addScrollId(scrollId);
        try {
            restHighLevelClient.clearScroll(clearScrollRequest, RequestOptions.DEFAULT);
        } catch (IOException e) {
            log.error("[Elasticsearch] >> clearScroll exception,scrollId:{}", scrollId, e);
        }
    }

    private SearchResponse getScrollResponse(String[] indexArray, QueryBuilder queryBuilder, Integer size,
                                             String[] includes, String[] excludes, String scrollId, Long keepAliveSeconds) throws IOException {
        SearchResponse searchResponse;
        Scroll scroll = new Scroll(TimeValue.timeValueSeconds(keepAliveSeconds));

        if (StringUtils.isEmpty(scrollId)) {
            SearchSourceBuilder searchSourceBuilder = SearchSourceBuilder
                    .searchSource()
                    .query(queryBuilder)
                    .size(size)
                    .fetchSource(includes, excludes);

            SearchRequest searchRequest = new SearchRequest()
                    .indices(indexArray)
                    .scroll(scroll)
                    .source(searchSourceBuilder);
            searchResponse = this.restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        } else {
            SearchScrollRequest scrollRequest = new SearchScrollRequest(scrollId);
            scrollRequest.scroll(scroll);
            searchResponse = this.restHighLevelClient.scroll(scrollRequest, RequestOptions.DEFAULT);
        }

        return searchResponse;
    }

}
