package com.jourwon.spring.boot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author JourWon
 * @date 2022/1/18
 */
@SpringBootApplication
public class SpringBootValidatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootValidatorApplication.class, args);
    }

}
