package com.jourwon.spring.boot.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author JourWon
 * @date 2022/3/20
 */
@Data
@ApiModel("插件ID请求体")
public class PluginIdRequest {

    @ApiModelProperty("插件ID")
    @NotBlank(message = "插件ID不能为空")
    private String pluginId;

}
