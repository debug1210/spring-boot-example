package com.jourwon.spring.boot;

/**
 * 超能力 接口
 *
 * @author JourWon
 * @date 2021/9/14
 */
public interface SupernaturalAbility {

    /**
     * 英雄名称
     *
     * @return String 英雄名称
     */
    String heroName();

    /**
     * 吃东西
     */
    void eat();

    /**
     * 说话
     */
    void say();

    /**
     * 走路
     */
    void walk();

    /**
     * 跑步
     */
    void run();

    /**
     * 攻击
     */
    void attack();

    /**
     * 防御
     */
    void defense();

}
