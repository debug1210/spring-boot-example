package com.jourwon.spring.boot.service;

import com.jourwon.spring.boot.model.dto.ConfigInfo;

/**
 * ConfigService
 *
 * @author JourWon
 * @date 2021/12/22
 */
public interface ConfigService {

    /**
     * 获取配置信息
     *
     * @return {@link ConfigInfo}
     */
    ConfigInfo configInfo();

}
