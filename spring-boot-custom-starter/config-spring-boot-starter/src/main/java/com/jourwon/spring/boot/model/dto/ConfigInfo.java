package com.jourwon.spring.boot.model.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * 配置信息
 *
 * @author JourWon
 * @date 2021/12/21
 */
@Data
@Builder
public class ConfigInfo implements Serializable {

    private static final long serialVersionUID = -2878523532668902073L;

    /**
     * ID
     */
    private String id;

    /**
     * IP地址
     */
    private String ip;

}
