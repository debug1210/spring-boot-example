package com.jourwon.spring.boot.config;

import com.jourwon.spring.boot.properties.ConfigProperties;
import com.jourwon.spring.boot.service.ConfigService;
import com.jourwon.spring.boot.service.impl.ConfigServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;

/**
 * 自动配置类
 *
 * @author JourWon
 * @date 2021/12/21
 */
@Slf4j
@Configuration
@EnableConfigurationProperties(value = ConfigProperties.class)
public class ConfigAutoConfiguration {

    @Resource
    private ConfigProperties properties;

    /**
     * 配置ExampleService
     *
     * @return {@link ConfigService}
     */
    @Bean
    @ConditionalOnMissingBean
    public ConfigService configService() {
        log.info("Config ConfigService Start...");
        ConfigService service = new ConfigServiceImpl(properties.getId(), properties.getIp());
        log.info("Config ConfigService End.");
        return service;
    }

}
