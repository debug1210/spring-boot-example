package com.jourwon.spring.boot.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * 配置属性项
 *
 * @author JourWon
 * @date 2021/12/21
 */
@Data
@ConfigurationProperties(value = "com.jourwon.config")
public class ConfigProperties {

    /**
     * ID标识
     */
    private String id;

    /**
     * IP地址
     */
    private String ip;

}
