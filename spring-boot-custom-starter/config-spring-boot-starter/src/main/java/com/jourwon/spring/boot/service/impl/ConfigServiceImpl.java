package com.jourwon.spring.boot.service.impl;

import com.jourwon.spring.boot.model.dto.ConfigInfo;
import com.jourwon.spring.boot.service.ConfigService;

/**
 * ConfigServiceImpl
 *
 * @author JourWon
 * @date 2021/12/22
 */
public class ConfigServiceImpl implements ConfigService {

    /**
     * ID
     */
    private String id;
    /**
     * ip
     */
    private String ip;

    /**
     * 构造函数
     *
     * @param id ID
     * @param ip IP
     */
    public ConfigServiceImpl(String id, String ip) {
        this.id = id;
        this.ip = ip;
    }

    /**
     * 获取配置信息
     *
     * @return {@link ConfigInfo}
     */
    @Override
    public ConfigInfo configInfo() {
        return ConfigInfo.builder().id(this.id).ip(this.ip).build();
    }

}
