package com.jourwon.spring.boot.test.controller;

import com.alibaba.fastjson.JSON;
import com.jourwon.spring.boot.service.ConfigService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * ConfigInfoController
 *
 * @author JourWon
 * @date 2021/12/23
 */
@RestController
public class ConfigInfoController {

    @Resource
    private ConfigService configService;

    @GetMapping("/configInfo")
    public String configInfo() {
        return JSON.toJSONString(configService.configInfo());
    }

}
