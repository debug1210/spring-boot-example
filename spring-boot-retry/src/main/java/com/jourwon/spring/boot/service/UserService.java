package com.jourwon.spring.boot.service;

import com.jourwon.spring.boot.model.vo.UserVO;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;

import java.util.List;

/**
 * 用户service接口
 *
 * @author JourWon
 * @date 2021/2/5
 */
public interface UserService {

    /**
     * 查询所有的用户列表
     *
     * @return
     */
    List<UserVO> listUsers();

    /**
     * 查询所有的用户列表
     *
     * @return
     */
    @Retryable(
            // 仅当方法抛出Exception时才尝试重试
            value = {Exception.class},
            // 最大重试次数，总调用次数
            maxAttempts = 4,
            // 重试补偿机制，默认没有，初始延迟2秒，然后之后验收2倍延迟重试，第一次重试2秒，第二次2*2秒，第三次2*2*2秒
            backoff = @Backoff(delay = 2000L, maxDelay = 30000L, multiplier = 2L),
            // 配置回调方法名称
            listeners = "retryListener"
    )
    List<UserVO> listUsersAnotation();

}
