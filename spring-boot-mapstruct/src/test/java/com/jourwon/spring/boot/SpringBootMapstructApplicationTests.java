package com.jourwon.spring.boot;

import com.alibaba.fastjson.JSON;
import com.jourwon.spring.boot.dto.DoctorDto;
import com.jourwon.spring.boot.entity.Doctor;
import com.jourwon.spring.boot.mapper.DoctorMapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Slf4j
@SpringBootTest
class SpringBootMapstructApplicationTests {

    @Test
    void contextLoads() {
    }

    // @Test
    // public void testDoctorToDoctorDto() {
    //     Doctor doctor = Doctor.builder().id(1).name("张三丰").build();
    //     DoctorDto doctorDto = DoctorMapper.INSTANCE.doctorToDoctorDto(doctor);
    //     String result = JSON.toJSONString(doctorDto);
    //     log.info(result);
    // }
    //
    // @Test
    // public void testToDto() {
    //     Doctor doctor = Doctor.builder().id(1).name("张三丰").specialty("航天员").build();
    //     DoctorDto doctorDto = DoctorMapper.INSTANCE.toDto(doctor);
    //     String result = JSON.toJSONString(doctorDto);
    //     log.info(result);
    // }
    //
    // @Test
    // public void testToDtoWithMultiSource() {
    //     Doctor doctor = Doctor.builder().id(1).name("张三丰").specialty("航天员").build();
    //     Education education = Education.builder().degreeName("博士").build();
    //     DoctorDto doctorDto = DoctorMapper.INSTANCE.toDtoWithMultiSource(doctor, education);
    //     String result = JSON.toJSONString(doctorDto);
    //     log.info(result);
    // }
    //
    // @Test
    // public void testToDtoWithSubObject() {
    //     List<Patient> list = new ArrayList<Patient>() {
    //         {
    //             add(Patient.builder().id(1).name("阿蒙").build());
    //             add(Patient.builder().id(2).name("阿兰").build());
    //         }
    //     };
    //     Doctor doctor = Doctor.builder().id(1).name("张三丰").specialty("航天员").patientList(list).build();
    //     DoctorDto doctorDto = DoctorMapper.INSTANCE.toDtoWithSubObject(doctor);
    //     String result = JSON.toJSONString(doctorDto);
    //     log.info(result);
    // }
    //
    // @Test
    // public void testUpdateModel() {
    //     List<Patient> list = new ArrayList<Patient>() {
    //         {
    //             add(Patient.builder().id(1).name("阿蒙").build());
    //             add(Patient.builder().id(2).name("阿兰").build());
    //         }
    //     };
    //     Doctor doctor = Doctor.builder().id(1).name("张三丰").specialty("航天员").patientList(list).build();
    //     DoctorDto doctorDto = DoctorMapper.INSTANCE.toDtoWithSubObject(doctor);
    //
    //     Doctor updateModel = new Doctor();
    //     DoctorMapper.INSTANCE.updateModel(doctorDto, updateModel);
    //     String result = JSON.toJSONString(updateModel);
    //     log.info(result);
    // }
    //
    // @Test
    // public void testToModel() {
    //     PatientDto dto = PatientDto.builder().id(1).name("阿蒙").dateOfBirth(LocalDate.now()).build();
    //     Patient patient = PatientMapper.INSTANCE.toModel(dto);
    //     String result = JSON.toJSONString(patient);
    //     log.info(result);
    // }
    //
    // @Test
    // public void testMap() {
    //     List<Education> list = new ArrayList<Education>() {{
    //         add(Education.builder().degreeName("博士").build());
    //         add(Education.builder().degreeName("研究生").build());
    //     }};
    //
    //     List<EducationDto> map = EducationMapper.INSTANCE.map(list);
    //     String result = JSON.toJSONString(map);
    //     log.info(result);
    // }

    // @Test
    // public void testSetConvert() {
    //     Set<Doctor> list = new HashSet<Doctor>() {{
    //         add(Doctor.builder().id(1).name("Rocket").build());
    //         add(Doctor.builder().id(2).name("Mess").build());
    //     }};
    //
    //     Set<DoctorDto> dtoSet = DoctorMapper.INSTANCE.setConvert(list);
    //     String result = JSON.toJSONString(dtoSet);
    //     log.info(result);
    // }
    //
    // @Test
    // public void testMapConvert() {
    //     Map<String, Doctor> map = new HashMap<String, Doctor>() {{
    //         put("1", Doctor.builder().id(1).name("Rocket").build());
    //         put("2", Doctor.builder().id(2).name("Mess").build());
    //     }};
    //
    //     Map<String, DoctorDto> dtoMap = DoctorMapper.INSTANCE.mapConvert(map);
    //     String result = JSON.toJSONString(dtoMap);
    //     log.info(result);
    // }


}
