package com.jourwon.spring.boot.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 传输对象
 *
 * @author JourWon
 * @date 2023/4/2
 */
@Getter
@Setter
@NoArgsConstructor
public class DoctorDto {

    /**
     * id
     */
    private Integer id;

    /**
     * 姓名
     */
    private String name;

    /**
     * 学历
     */
    private String degree;

    /**
     * 专业
     */
    private String specialization;

    /**
     * 患者列表
     */
    private List<PatientDto> patientDtoList;

    private String externalId;

    private LocalDateTime availability;

}
