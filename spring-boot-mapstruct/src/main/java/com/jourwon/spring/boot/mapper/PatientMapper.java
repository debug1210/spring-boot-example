package com.jourwon.spring.boot.mapper;

import com.jourwon.spring.boot.dto.PatientDto;
import com.jourwon.spring.boot.entity.Patient;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

/**
 * 映射器
 *
 * @author JourWon
 * @date 2023/4/2
 */
@Mapper
public interface PatientMapper {

    PatientMapper INSTANCE = Mappers.getMapper(PatientMapper.class);

    /**
     //  * patient转换为PatientDto
     //  *
     //  * @param patient patient
     //  * @return PatientDto PatientDto
     //  */
    // PatientDto toDto(Patient patient);

    /**
     * 实体类转换方法
     *
     * @param patientDto patientDto
     * @return Patient
     */
    @Mapping(source = "dateOfBirth", target = "dateOfBirth", dateFormat = "dd/MM/yyyy")
    Patient toModel(PatientDto patientDto);

    /**
     * 实体类转换方法
     *
     * @param patient patient
     * @return PatientDto
     */
    @InheritInverseConfiguration
    PatientDto toDto(Patient patient);

}
