package com.jourwon.spring.boot.entity;

import lombok.*;

import java.util.List;

/**
 * @author JourWon
 * @date 2023/4/3
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Hospital {

    private List<Doctor> doctors;

}
