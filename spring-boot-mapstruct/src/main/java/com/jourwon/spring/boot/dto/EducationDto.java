package com.jourwon.spring.boot.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author JourWon
 * @date 2023/4/3
 */
@Getter
@Setter
@NoArgsConstructor
public class EducationDto {

    /**
     * 学历
     */
    private String degreeName;

    /**
     * 学院
     */
    private String institute;

    private String years;

}
