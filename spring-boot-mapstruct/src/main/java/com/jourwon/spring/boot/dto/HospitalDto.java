package com.jourwon.spring.boot.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author JourWon
 * @date 2023/4/3
 */
@Getter
@Setter
@NoArgsConstructor
public class HospitalDto {

    private List<DoctorDto> doctors;

    public void addDoctor(DoctorDto doctorDTO) {
        if (doctors == null) {
            doctors = new ArrayList<>();
        }

        doctors.add(doctorDTO);
    }

}
